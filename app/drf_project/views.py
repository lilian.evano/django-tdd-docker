# app/drf_project/views.py

from django.http import JsonResponse


def ping(request):
    data = {"ping": "pongs"}
    return JsonResponse(data)
